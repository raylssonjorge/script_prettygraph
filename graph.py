import pandas as pd
import matplotlib.pyplot as plt
from pylab import rcParams
import numpy as np
from numpy import linspace
from scipy import optimize



# Draw Plot
rcParams['figure.figsize'] = 12, 6



rcParams['figure.figsize'] = 12, 6   #size of the figure

rcParams['axes.linewidth'] = 2 # set the value globally



SMALL_SIZE = 14
MEDIUM_SIZE = 18
BIGGER_SIZE =  18

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  #


import sys
filename = sys.argv[1] #filename
n = int(sys.argv[2])  #order of the function
cl = (sys.argv[3])  #color
xa = sys.argv[4]   #x-axis
ya = sys.argv[5]  #y-axis
t = sys.argv[6]   #title





data = pd.read_csv(filename, sep='\s+',header=None,comment='@')

data = pd.DataFrame(data)  # vai ler o arquivo usando panda no formato Data frame
x = data[0] # escolhi a primeira coluna como x
y = data[1] # segunda coluna como y


plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')
plt.rc('font',**{'family':'serif','serif':['Palatino']})



def f2(x, a, b, c,d):



    y =  a*x**2+ b + c*x
    return y



def f1(x, a, b, c,d):



    y =  a*x +  b
    return y



def f3(x, a, b, c, d):

    y= a*x**3 + b*x**2 + c*x +d
    return y



def fexp(x,a,b,c,d):
    y =   a*np.exp(b*x) + c
    return y



# This will look if the order matches and choose the right funcion
def week(order):
        switcher={
                1:f1,
                2:f2,
                3:f3,
                4:fexp
             }
        return switcher.get(order)











#this will optimize the funcion and give and return the right coeficients



popt, pcov = optimize.curve_fit(week(n), x, y)
a, b, c,d = popt

xs = linspace(x[:1] -1, x[-1:]+1, 256)
ys = week(n)(xs, a, b, c,d)











# Decoration
#plt.ylim(0.1, 11)
#plt.xlim(-0.2, 7)


plt.plot(x, y,
         color="seagreen",marker='.',linestyle='',linewidth=2,
         markersize=12)
plt.plot(xs, ys, color=cl,linewidth=3.5,label="regression")



# Remove borders



plt.gca().spines["top"].set_alpha(0.0)
plt.gca().spines["bottom"].set_alpha(0.8)
plt.gca().spines["right"].set_alpha(0.0)
plt.gca().spines["left"].set_alpha(0.8)
plt.gca().xaxis.set_ticks_position('none')
plt.gca().yaxis.set_ticks_position('none')

plt.ylabel(ya)
plt.xlabel(xa)
plt.title(t)


legend = plt.legend(loc=4)

legend.get_frame().set_facecolor('none')


legend.get_frame().set_linewidth(0.0)

#if you wish to save the figure uncomment bellow

plt.savefig("final.png", dpi=600, bbox_inches='tight')

plt.show()
